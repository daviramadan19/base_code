@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="portlet green-sharp box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings"></i>
                    <small>managemen data {{ $title }}</small>
                </div>
                <div class="actions">
                    <a class="btn red btn-outline btn-circle" href="{{ route($view.'.index') }}">
                        <i class="fa fa-arrow-left"></i>
                        <span class="hidden-xs"> Back</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table display table-striped table-bordered table-hover ">
                    <thead>
                        <tr>
                            <th>Nama Barang</th>
                            <th>Tanggal Beli</th>
                            <th>Harga</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                <tbody>

                </tbody>
            </table>
            </div>
        </div>
    </div>

</div>
@endsection
