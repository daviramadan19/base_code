@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="portlet green-sharp box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings"></i>
                    <span class="caption-subject font-white sbold uppercase">{{ $title }}</span>
                    <small>managemen data {{ $title }}</small>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title="">
                        <i class="fa-plus"></i>
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail Transaksi</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" action="{{ url('installments/store') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <input type="text" name="payment_date" class="form-control datepicker" value="">
                                                <label for="form_control_1">Tanggal Pembayaran</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <input type="hidden" name="order_id" class="form-control" value="{{ $data->id }}">
                                                <input type="text" name="value" class="form-control" value="">
                                                <label for="form_control_1">Pembayaran</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn green" type="submit">Save</button>

                                </form>
                                <table class="table table-responsive">
                                    <tr>
                                        <th>Tanggal Pembayaran</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    @foreach($data->installments as $installment)
                                    <tr>
                                        <td>{{ $installment->payment_date }}</td>
                                        <td>{{ number_format($installment->value) }}</td>
                                    </tr>
                                    @php
                                    @$total += $installment->value;
                                    @endphp
                                    @endforeach
                                    <tr>
                                        <td>Total Terbayar : </td>
                                        <td>{{ number_format(@$total) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Pembayaran : </td>
                                        <td>{{ number_format(@$data->order_details->sum('price')) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sisa Pembayaran : </td>
                                        <td>{{ number_format(@$data->order_details->sum('price')-@$total) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <form method="post" action="{{ url('/'.$view.'/'.$data->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Transaksi</div>
                        <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input has-success">
                                    <label for="form_control_1">Customer :</label>
                                    {{ @$data->customer_id }}
                                </div>
                                <div class="form-group form-md-line-input has-success">
                                    <label for="form_control_1">Penjual :</label>
                                    {{ @$data->seller_id }}
                                </div>
                                <div class="form-group form-md-line-input has-success">
                                    <label for="form_control_1">Number Invoice :</label>
                                    {{ $data->number_invoice }}
                                </div>
                                <div class="form-group form-md-line-input has-success">
                                    <label for="form_control_1">Order Date :</label>
                                    {{ @$data->order_date }}
                                </div>
                                <div class="form-group form-md-line-input has-success">
                                    <label for="form_control_1">Delivery Date</label>
                                    {{ @$data->delivery_date }}
                                </div>
                                <div class="form-group form-md-line-input has-success">
                                    <label for="form_control_1">Order Address</label>
                                    {{ @$data->order_address }}
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="panel panel-default">
                                    <div class="panel-heading">Detail Transaksi</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-hover table-light">
                                                    <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Qty</th>
                                                        <th>Price</th>
                                                        <th>Subtotal</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="detail">
                                                    @if(!empty($data->order_details))
                                                        @foreach($data->order_details as $detail)
                                                        @php
                                                        $subtotal = @$detail->qty * @$detail->price;
                                                        @$total += $subtotal;
                                                        @endphp
                                                        <tr>
                                                            <td style="width: 30%">
                                                                {{ @$detail->product_id }}
                                                            </td>
                                                            <td style="width: 10%">
                                                                {{ @$detail->qty }}
                                                            </td>
                                                            <td>
                                                                {{ @$detail->price }}
                                                            </td>
                                                            <td>
                                                                {{ @$subtotal }}
                                                            </td>
                                                        </tr>

                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="3" align="right">Total:</td>
                                                            <td>{{ @$total }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn green" type="submit">Save</button>
                    <a class="btn red"> Cancel </a>

                    @section('js')
                    <script>
                    $(document).ready(function() {

                        $("body").on('change','.product_id', function(){
                            var index       = $(this).index(".product_id");
                            var produk_id     = $(this).val();
                            $( ".price" ).eq(index).val(0);
                            if(produk_id > 0)
                            {
                                var url = "{{ url('api/products') }}"+"/"+produk_id;
                                $.get( url, function( data ) {
                                    console.log(data.data.price);
                                    if(data.data.price > 0)
                                    {
                                        $(".price").eq(index).val(data.data.price);
                                        
                                    }
                                }, "json" );
                            }
                            subtotal(index);
                        });

                        $("body").on('click','.hapus', function(){
                            $('.hapus').eq($(this).index()).parent().parent().remove();
                            total();
                        });

                        $("body").on('keyup','.qty', function(){
                            var index       = $(this).index(".qty");
                            subtotal(index);
                        });

                        function subtotal(index){
                            var qty     = $(".qty").eq(index).val();
                            var price   = $(".price").eq(index).val();
                            $(".subtotal").eq(index).val(qty * price);
                            total();
                        }

                        function total(){
                            var sum = 0;
                            $('.subtotal').each(function(){
                                sum += parseFloat($(this).val());  // Or this.innerHTML, this.innerText
                            });
                            $('.total').val(sum);
                        }

                        $("#add_row").click(function(){
                            var url = "{{ url('orders/detail') }}";
                            $.get( url, function( data ) {
                                $("#detail").append( data );
                                $('select').select2();
                            });
                            
                        })
                    });
                    </script>
                    @endsection
                </form>
            </div>
        </div>

    </div>
</div>
@endsection