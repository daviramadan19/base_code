<div class="row">
    <div class="col-md-6 ">
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="username" value="{{ @$data->name }}">
            <label>Username</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="password" name="password" value="{{ @$data->password }}">
            <label>Password</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            {!! Template::selectbox($listSeller,@$data->seller_id,"seller_id",[ 'class' => 'form-control select2' ]) !!}
            <label>Seller</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            {!! Template::selectbox($listGallery,@$data->gallery_id,"gallery_id",[ 'class' => 'form-control select2' ]) !!}
            <label>Gallery</label>
        </div>          
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="name" value="{{ @$data->name }}">
            <label>Nama</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="address" value="{{ @$data->address }}">
            <label>Alamat</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="phone_number" value="{{ @$data->phone_number }}">
            <label>Nomor Telepon</label>
        </div>
    </div>
</div>
<button class="btn green" type="submit">Save</button>
<button class="btn red"> Cancel </button>
