<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $fillable = [
        'user_id', 
        'seller_id',
        'gallery_id', 
        'name',
        'address',
        'phone_number',
    ];

    public function user()	
    {
        return $this->belongsTo('App\Models\User');
    }

    public function seller()	
    {
        return $this->belongsTo('App\Models\Seller');
    }

    public function gallery()	
    {
        return $this->belongsTo('App\Models\Gallery');
    }
}
