<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = [
        'supplier_id', 
        'number_invoice',
        'purchase_date',
        'purchase_address'
    ];

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }

    public function purchase_details()
    {
        return $this->hasMany('App\Models\PurchaseDetail' , 'purchase_id');
    }

    
}
