<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyOrderDetail extends Model
{   
    protected $fillable = [
        'order_id',
        'product_id',
        'qty', 
        'price',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
