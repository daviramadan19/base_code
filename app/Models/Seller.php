<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
	protected $fillable = [
        'gallery_id', 
        'name',
    ];

    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery');
    }
}
