<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    protected $fillable = [
        'order_id',
        'payment_date',
        'value'
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}
