<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
	protected $table = 'sales';

    protected $fillable = [
        'galeri_id', 
        'nama'
    ];
}
