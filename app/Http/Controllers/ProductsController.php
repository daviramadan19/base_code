<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Unit;
use App\Models\ProductCategory;
use App\Traits\ImagesTrait;
use Yajra\DataTables\Facades\DataTables;
use DB,General,View,JsValidator, Excel;

class ProductsController extends Controller
{
    use ImagesTrait;

    public $view;
    public $main_model;

    public function __construct(Product $main_model){
        $this->view         = 'products';
        $this->title        = 'Produk';
        $this->main_model   = $main_model;
        $this->validate     = 'ProductRequest';
        
        $listUnit                   = Unit::pluck('name','id');
        $listProductCategory        = ProductCategory::pluck('name','id');

        View::share('listProductCategory', $listProductCategory);
        View::share('listUnit', $listUnit);
        View::share('view', $this->view);
        View::share('title', $this->title);
    }

    public function index(Request $request)
    {
        
        $columns = [
            'product_category_name',
            'unit_name',
            'name',
            'price',
            'stock',
            'action',
        
        ];

        if($request->ajax())
        {
            $datas = $this->main_model->select(['*']);
            return Datatables::of($datas)
                ->addColumn('product_category_name',function($data){
                    return $data->product_category->name;
                })
                ->addColumn('unit_name',function($data){
                    return $data->unit->name;
                })
                ->addColumn('action',function($data){
                        return view('page.'.$this->view.'.action',compact('data'));
                    })
              
                ->make(true);
        }
        return view('page.'.$this->view.'.index')
            ->with(compact('datas','columns'));

    }

    public function ProductExport(){
        $product = $this->main_model->select(['*'])->get();
        return Excel::create('data_produk', function($excel) use ($product){
            $excel->sheet('mysheet', function($sheet) use ($product){
                $sheet->fromArray($product);
            });

        })->download('xlsx');
    }

    
    public function create()
    {
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.create')->with(compact('validator'));
    }

    public function store(ProductRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try{
            $input['image'] = General::setImage($request->file('image'),$this->view);
            $data = $this->main_model->create($input);
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = $this->main_model->findOrFail($id);
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.edit')->with(compact('validator','data'));
    }

    public function update(ProductRequest $request, $id)
    {
        $input = $request->all();
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $input['image'] = General::setImage($request->file('image'),$this->view);            
            $data->fill($input)->save();
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $data->delete();
            DB::commit();
            toast()->success('Data berhasil hapus', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }
}
