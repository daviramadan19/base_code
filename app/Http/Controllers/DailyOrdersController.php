<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DailyOrderRequest;

use App\Models\DailyOrder;
use App\Models\DailyOrderDetail;
use App\Models\Customer;
use App\Models\Seller;
use App\Models\Product;

use App\Traits\ImagesTrait;
use Yajra\DataTables\Facades\DataTables;
use DB,General,View,JsValidator,Hash,Alert;

class DailyOrdersController extends Controller
{
    use ImagesTrait;

    public $view;
    public $main_model;

    public function __construct(DailyOrder $main_model){
        $this->title        = 'Form Bulanan';
        $this->view         = 'daily_orders';
        $this->main_model   = $main_model;
        $this->validate     = 'DailyOrderRequest';

        $listCustomer       = Customer::orderBy('name','asc')->pluck('name', 'id');
        $listSeller         = Seller::orderBy('name','asc')->pluck('name', 'id');
        $listProduct        = Product::orderBy('name','asc')->pluck('name', 'id');

        View::share('view', $this->view);
        View::share('title', $this->title);

        View::share('listCustomer', $listCustomer);
        View::share('listSeller', $listSeller);
        View::share('listProduct', $listProduct);
    }

    public function index(Request $request)
    {
        $columns = ['seller.name', 'recomendation_date', 'order_address', 'action'];
        if($request->ajax())
        {
            $datas = $this->main_model->with(['customer', 'seller'])->get();
            return Datatables::of($datas)
                ->addColumn('action',function($data){
                        return view('page.'.$this->view.'.action',compact('data'));
                    })
                ->escapeColumns(['actions'])
                ->make(true);
        }
        return view('page.'.$this->view.'.index')
            ->with(compact('datas','columns'));
    }

    public function create()
    {
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.create')->with(compact('validator'));
    }

    public function store(DailyOrderRequest $request)
    {
        $input = $request->all();
        
        DB::beginTransaction();
        try {
            $input['image'] = General::setImage($request->file('image'),$this->view);            
            $data = $this->main_model->create($input);
            foreach ($input['product_id'] as $k => $v) {
                $input_detail['order_id']       = $data->id;
                $input_detail['product_id']     = $input['product_id'][$k];
                $input_detail['qty']            = $input['qty'][$k];
                $input_detail['price']         = $input['price'][$k];
                DailyOrderDetail::create($input_detail);
                $product    = Product::findOrFail($input['product_id'][$k]);
                $stock      = $product->stock - $input['qty'][$k];
                $update_stock['stock'] = $stock;
                $product->fill($update_stock)->save();
            }
            // $this->print_dotmatrix($input);
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            // return redirect()->route($this->view.'.index');
        } catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function show($id)
    {
        $data = $this->main_model->findOrFail($id);
        return view('page.'.$this->view.'.show')->with(compact('data'));
    }

    public function edit($id)
    {
        $data = $this->main_model->findOrFail($id);
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.edit')->with(compact('validator','data'));
    }

    public function update(DailyOrderRequest $request, $id)
    {
        $input = $request->all();
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $input['image'] = General::setImage($request->file('image'),$this->view);            
            $data->fill($input)->save();
            foreach($data->order_details as $detail){
                $product    = Product::findOrFail($detail->product_id);
                $stock      = $product->stock + $detail->qty;
                $update_stock['stock'] = $stock;
                $product->fill($update_stock)->save();
            }
            DailyOrderDetail::whereOrderId($data->id)->delete();
            foreach ($input['product_id'] as $k => $v) {
                $product    = Product::findOrFail($input['product_id'][$k]);
                $stock      = $product->stock - $input['qty'][$k];
                $update_stock['stock'] = $stock;
                $product->fill($update_stock)->save();

                $input_detail['order_id']       = $data->id;
                $input_detail['product_id']     = $input['product_id'][$k];
                $input_detail['qty']            = $input['qty'][$k];
                $input_detail['price']          = $input['price'][$k];
                DailyOrderDetail::create($input_detail);
                
            }
            // $this->print_dotmatrix($input);
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $data->delete();
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function detail()
    {
        return view('page.'.$this->view.'.detail');
    }

    public function print_dotmatrix($input){
        $customer = Customer::whereId($input['customer_id'])->first();

        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed;
        $Data .= "============================================\n";
        $Data .= "|          Aliya Store       |\n";
        $Data .= $customer->name."\n";
        $Data .= $input['number_invoice']."/".date('d-m-Y H:i:s')."\n";
        $Data .= "============================================\n";
        foreach ($input['product_id'] as $k => $v) {
            @$no++;
            $subtotal = $input['qty'][$k] * $input['price'][$k];
            @$total += $subtotal;
            $product = Product::whereId($input['product_id'][$k])->first();
            $Data .= $no."  ".$product->name."\n";
            $Data .=
                    str_pad(number_format($input['qty'][$k]).' '.$product->unit->name, 8, " ", STR_PAD_LEFT).
                    " x ".
                    str_pad(number_format($input['price'][$k]), 8, " ", STR_PAD_LEFT).
                    str_pad(number_format($subtotal), 10, " ", STR_PAD_LEFT)."\n";

        }
        $Data .= "============================================\n";
        $Data .= str_pad("Total : ".number_format($total), 29, " ", STR_PAD_LEFT)."\n";
        $Data .= "============================================\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        fwrite($handle, $Data);
        fclose($handle);
        copy($file, "//localhost/xprinter");  # Lakukan cetak
        unlink($file);

    }

}
