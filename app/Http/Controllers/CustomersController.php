<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;

use App\Models\Customer;
use App\Models\Seller;
use App\Models\Gallery;
use App\Models\User;

use App\Traits\ImagesTrait;
use Yajra\DataTables\Facades\DataTables;
use DB,General,View,JsValidator;

class CustomersController extends Controller
{
    use ImagesTrait;

    public $view;
    public $main_model;

    public function __construct(Customer $main_model){
        $this->view         = 'customers';
        $this->title        = 'Pelanggan';
        $this->main_model   = $main_model;
        $this->validate     = 'CustomerRequest';

        $listSeller     = Seller::pluck('name','id');
        $listGallery    = Gallery::pluck('name','id');
        
        View::share('listSeller', $listSeller);
        View::share('listGallery', $listGallery);
        View::share('view', $this->view);
        View::share('title', $this->title);
    }

    public function index(Request $request)
    {
        
        $columns = [
            'username',
            'seller_name',
            'gallery_name',
            'name',
            'address',
            'phone_number',
            'action'
        ];

        if($request->ajax())
        {
            $datas = $this->main_model->select(['*']);
            return Datatables::of($datas)
                ->addColumn('username',function($data){
                    return @$data->user->username;
                })
                ->addColumn('seller_name',function($data){
                    return @$data->seller->name;
                })
                ->addColumn('gallery_name',function($data){
                    return @$data->gallery->name;
                })
                ->addColumn('action',function($data){
                        return view('page.'.$this->view.'.action',compact('data'));
                    })
                ->escapeColumns(['actions'])
                ->make(true);
        }
        return view('page.'.$this->view.'.index')
            ->with(compact('datas','columns'));

    }

    public function create()
    {
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.create')->with(compact('validator'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try{
            $save_user['username']  = $input['username'];
            $save_user['password']  = $input['password'];
            $user = User::create($save_user);

            $input['user_id'] = $user->id;
            $input['image'] = General::setImage($request->file('image'),$this->view);
            $data = $this->main_model->create($input);
            DB::commit();
            toast()->success('Data berhasil di input', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);
            DB::rollback();
        }
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = $this->main_model->findOrFail($id);
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.edit')->with(compact('validator','data'));
    }

    public function update(CustomerRequest $request, $id)
    {
        $input = $request->all();
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $save_user['username']  = $input['username'];
            $save_user['password']  = $input['password'];
            $user = User::create($save_user);

            $input['user_id'] = $user->id;
            $input['image'] = General::setImage($request->file('image'),$this->view);
            $data->fill($input)->save();

            DB::commit();
            toast()->success('Data berhasil di hapus', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);
            DB::rollback();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $data->delete();
            DB::commit();
            toast()->success('Data berhasil di hapus', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);
            DB::rollback();
        }
        return redirect()->back();
    }
}
