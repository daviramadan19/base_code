<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


 Route::group(['middleware' => ['menu','auth']], function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('dashboard');
    Route::resource('suppliers', 'SuppliersController');
    Route::post('role_menus/mass_store', 'RoleMenusController@mass_store')->name('role_menus.mass_store');
    Route::get('roles/{id}/access', 'RolesController@access')->name('role.access');
    Route::resource('roles', 'RolesController');
    Route::resource('customers', 'CustomersController');
    Route::resource('menus', 'MenusController');
    Route::resource('galleries', 'GalleriesController');
    Route::resource('units', 'UnitsController');
    Route::resource('sellers', 'SellersController');
    Route::resource('product_categories', 'ProductCategoriesController');
    Route::resource('products', 'ProductsController');
    Route::get('export', 'ProductsController@ProductExport')->name('products.export');
     Route::get('purchases/report/{month}/{year}', 'PurchasesController@report')->name('purchases.report');
     Route::get('purchases/detail', 'PurchasesController@detail')->name('purchases.detail');
     Route::get('purchases/report_pembelian', 'PurchasesController@report_pembelian')->name('purchases.report_pembelian');
    Route::resource('purchases', 'PurchasesController');
     Route::get('orders/report/{month}/{year}', 'OrdersController@report')->name('orders.report');
     Route::get('orders/report', 'OrdersController@report')->name('orders.report');
     Route::get('orders/delivery_order/{id}', 'OrdersController@delivery_order')->name('orders.delivery_order');
    Route::get('orders/detail', 'OrdersController@detail')->name('orders.detail');
    Route::get('daily_orders/detail', 'DailyOrdersController@detail')->name('daily_orders.detail');
    Route::resource('orders', 'OrdersController');
    Route::resource('daily_orders', 'DailyOrdersController');
    Route::resource('instalments', 'OrdersController');
    Route::post('installments/store', 'InstallmentsController@store')->name('installments.store');
    Route::resource('role_menus', 'RoleMenusController', [
        'names' => [
            'store'   => 'role_menus.store'
        ],
    ]);
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('settings', 'SettingsController');
 });
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();
